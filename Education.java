/*SRM196 DIV2 - 250

Problem Statement
    
Even students who hate math find one calculation very useful -- what is the lowest score I can get on the last test and pull out a certain grade? Let's write a program to help them minimize their education.
We will assume that an average score of 90 or higher is rewarded with an A grade, 80 or higher (but less than 90) is a B, 70 or higher (but less than 80) is a C, 60 or higher (but less than 70) is a D. All test scores are integers between 0 and 100 inclusive and the average is NOT rounded -- for example an average of 89.99 does NOT get you an A.
Create a class Education that contains a method minimize that is given a String desire indicating the desired grade and a int[] tests containing the scores on all but the final test. The method returns the lowest possible test score for the final test that will earn at least the desired grade. If even a perfect score won't get the desired grade, return -1.
The desired grade will be given as a String of length 1, either "A", "B", "C", or "D".
Definition
    
Class:
Education
Method:
minimize
Parameters:
String, int[]
Returns:
int
Method signature:
int minimize(String desire, int[] tests)
(be sure your method is public)
Limits
    
Time limit (s):
2.000
Memory limit (MB):
64
Constraints
-
desire will be "A", "B", "C", or "D"
-
tests will contain between 0 and 20 elements inclusive.
-
Each element of tests will be between 0 and 100 inclusive.
Examples
0)

    
"A"
{0,70}
Returns: -1
Even a perfect 100 on the last test will only produce an average score of 56.66 so it is not possible to earn an A.
1)

    
"D"
{100,100,100,100,100,100}
Returns: 0
Nice scores! Even the worst possible score of 0 will give an average of 85.7 earning a B which satisfies your meager desire.
2)

    
"B"
{80,80,80,73}
Returns: 87
An 87 added to these scores will just exactly improve your average from 78.25 to 80.
3)

    
"B"
{80,80,80,73,79}
Returns: 88

4)

    
"A"
{80}
Returns: 100

This problem statement is the exclusive and proprietary property of TopCoder, Inc. Any unauthorized use or reproduction of this information without the prior written consent of TopCoder, Inc. is strictly prohibited. (c)2003, TopCoder, Inc. All rights reserved.
*/


import java.util.*;

public class Education{
	public static int minimize(String desire, int[] tests){
		HashMap<String, Integer> scores = new HashMap<String, Integer>();
		scores.put("A", 90);
		scores.put("B", 80);
		scores.put("C", 70);
		scores.put("D", 60);
		
		int sum = 0;
		
		int output;
		for (int i=0; i < tests.length; i++){
			sum += tests[i];
		}
		
		output = (tests.length + 1) * scores.get(desire) - sum;
		
		if (output <= 0)
			return 0;
		else if (output <= 100 )
			return output;
		else 
			return -1;
		
	}
	
	public static void main(String args[]){
		System.out.println(minimize("A",new int[]{0, 70}));
		System.out.println(minimize("D",new int[]{100,100,100,100,100,100}));
		System.out.println(minimize("B",new int[]{80,80,80,73}));
		System.out.println(minimize("B",new int[]{80,80,80,73,79}));
		System.out.println(minimize("A",new int[]{80}));
		
	}
	
}